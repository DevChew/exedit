<?php
/*
Plugin Name: exedit
Plugin URI:
Version: 0.0.1
Author: DevChew
License: GNU GPL 2+
*/

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

define( 'PM4WP_DIR', plugin_dir_path( __FILE__ ) );
define( 'PM4WP_INC_DIR', trailingslashit( PM4WP_DIR . 'inc' ) );

define( 'PM4WP_URL', plugin_dir_url( __FILE__ ) );
define( 'PM4WP_CSS_URL', trailingslashit( PM4WP_URL . 'css' ) );
define( 'PM4WP_JS_URL', trailingslashit( PM4WP_URL . 'js' ) );

define( 'PM4WP_INBOX_PAGE_ID', 18 );

if ( is_admin() )
{
	function get_fields_to_edit(){
		return array(
			'post_title',
			'post_content'
		);
	};
	function get_post_type_to_edit(){
		return 'post';
	}
	function get_posts_ids_to_edit(){
		return array(
			2, 4
		);
	};
	include_once PM4WP_INC_DIR . 'ajax.php';
	include_once PM4WP_INC_DIR . 'options.php';
}




