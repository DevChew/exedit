jQuery( document ).ready( function ( $ )
{
	function get_table_data(){
		var tableData = {};
		$('#exeditTable tbody tr').each( function( i ){
			var thisRow = {};
			$(this).find('input').each(function(field){
				thisRow[$(this).attr('name')] = $(this).val();
			});
			tableData[$(this).data('id')] = thisRow;
		})
		return tableData;
	}
	function pushData(id, data){
		console.log(id, data);
		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {
				action: 'exedit_update',
				id: id,
				data: data,
			},
			complete: function(msg){
				console.log(msg);
				 jQuery('#exeditTable tr[data-id="' + id + '"] .status').html(msg.responseText);
			},
			success : function(text)
			{
				console.log(text)
			}
		});
	}
	$('#save').on('click',function(){
		$.each(get_table_data(), pushData);
	})

} );
