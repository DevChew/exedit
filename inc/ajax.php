<?php
add_action('wp_ajax_exedit_update', 'exedit_update_ajax');
function exedit_update_ajax() {
    $id = $_POST['id'];
    $escaped_json = $_POST['data'];

    $post_meta = json_decode($escaped_json, true);
    $status = array();
    foreach($escaped_json as $post_field_key => $post_field_value){
        $status[$post_field_key] = update_metadata(get_post_type_to_edit(), $id, $post_field_key, $post_field_value, '');
    }

    //update_post_meta( $id, 'double_escaped_json', wp_slash( $escaped_json ) );
    
    //$escaped_json = '{"key":"value with \\"escaped quotes\\""}';
    //update_post_meta( $id, 'double_escaped_json', wp_slash( $escaped_json ) );


    die(json_encode($status));
}