<?php
add_action( 'admin_menu', 'exedit_admin_page_init' );

function exedit_admin_page_init()
{
    add_menu_page(
        'Exedit',     // page title
        'Exeditt',     // menu title
        'manage_options',   // capability
        'exedit',     // menu slug
        'exedit_admin_panel_render' // callback function
    );
}

function wpdocs_selectively_enqueue_admin_script( $hook ) {
	// wp_die($hook);
    if ( $hook != 'toplevel_page_exedit' ) {
        return;
    }
    wp_enqueue_script( 'my_custom_script', plugin_dir_url( __FILE__ ) . '../js/script.js', array(), '1.0' );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );

function exedit_admin_panel_render()
{
    global $title;

    print '<div class="wrap">';
	print "<h1>$title</h1>";
	?>
	<table id="exeditTable">
		<thead>
			<tr>
				<th>Post ID</th>
				<?php 
				foreach (get_fields_to_edit() as $field_name){
					echo '<th>' . $field_name . '</th>'; 
				}
				?>
			</tr>
		</thead>
		<tbody>
		
<?php
	$args = array(
		'posts_per_page'   => 5,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'page',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'author_name'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	);
	if($_GET['post_type']) { $args['post_type'] = $_GET['post_type']; };
	if($_GET['post_status']) { $args['post_status'] = $_GET['post_status']; };
	if($_GET['posts_per_page']) { $args['posts_per_page'] = $_GET['posts_per_page']; };
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<tr data-id="<?php echo $post->ID; ?>">
			<td class="post-id" ><?php echo $post->ID; ?></td>
			<?php 
			foreach (get_fields_to_edit() as $field_name){
				$value = get_metadata(get_post_type_to_edit(), $post->ID, $field_name, true);
				echo '<td><input type="text" name="' . $field_name . '" value="' . $value . '" /></td>';
			}
			?>
			<td class="status" >Loaded</td>
		</tr>
	<?php endforeach; 
	wp_reset_postdata();

	print '</tbody></table>
	<button id="save">Save</button></div>';
}